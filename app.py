from flask import Flask
from front import front_bp
from cms import cms_bp
from common import common_bp
import config
from exts import db
from flask_wtf import CSRFProtect

app = Flask(__name__)
app.config.from_object(config)
app.register_blueprint(front_bp)
app.register_blueprint(cms_bp)
app.register_blueprint(common_bp)
db.init_app(app)
CSRFProtect(app)


if __name__ == '__main__':
    app.run()
