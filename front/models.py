# -*- coding = utf-8 -*-
# @Time : 2020/7/29 9:47
# @Author : 贾伟文
# @File : models.py
# @Software : PyCharm

from exts import db
import shortuuid
from werkzeug.security import generate_password_hash, check_password_hash
import enum
from datetime import datetime


class FrontUser(db.Model):
    __tablename__ = 'front_user'
    id = db.Column(db.String(100), primary_key=True, default=shortuuid.uuid)
    username = db.Column(db.String(100), nullable=False)
    _password = db.Column(db.String(100), nullable=False)
    deposit = db.Column(db.Float, default=0)
    avatar = db.Column(db.String(100))

    @property
    def password(self):
        return self._password

    @password.setter
    def password(self, raw_password):
        self._password = generate_password_hash(raw_password)

    def check_password(self, raw_password):
        return check_password_hash(self.password, raw_password)


class BannerModel(db.Model):
    __tablename__='banner'
    id = db.Column(db.Integer,primary_key=True,autoincrement=True)
    name = db.Column(db.String(255),nullable=False)
    image_url = db.Column(db.String(255),nullable=False)
    link_url = db.Column(db.String(255),nullable=False)
    priority = db.Column(db.Integer,default=0)
    create_time = db.Column(db.DateTime,default=datetime.now)


class GenderEnum(enum.Enum):
    MALE = '1'
    FEMALE = '2'
    SECRET = '3'
    UNKNOWN = '4'


class PostModel(db.Model):
    __tablename__='post'
    id = db.Column(db.Integer,primary_key=True,autoincrement=True)
    title = db.Column(db.String(50),nullable=False)
    content = db.Column(db.Text,nullable=False)
    create_time = db.Column(db.DateTime,default=datetime.now)
    border_id = db.Column(db.Integer,db.ForeignKey('board.id'))
    user_id = db.Column(db.String(100),db.ForeignKey('front_user.id'))

    board = db.relationship('BoardModel',backref='posts')
    author = db.relationship(FrontUser,backref='posts')


class BoardModel(db.Model):
    __tablename__ = 'board'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(255), nullable=False)
    create_time = db.Column(db.DateTime, default=datetime.now)


class CommentModel(db.Model):
    __tablename__ = 'comment'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    content = db.Column(db.Text)
    post_id = db.Column(db.Integer, db.ForeignKey('post.id'))
    post = db.relationship(PostModel, backref='comments')
