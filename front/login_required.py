# -*- coding = utf-8 -*-
# @Time : 2020/7/28 21:38
# @Author : 贾伟文
# @File : login_required.py
# @Software : PyCharm
from flask import session, redirect, url_for


def login_required(func):
    def wrapper(*args, **kwargs):
        user_id = session.get("user_id")
        if not user_id:
            return redirect(url_for('front.login'))
        return func(*args, **kwargs)
    return wrapper
