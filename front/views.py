# -*- coding = utf-8 -*-
# @Time : 2020/7/29 8:44
# @Author : 贾伟文
# @File : views.py
# @Software : PyCharm

from flask import Blueprint, views, render_template, request, session, redirect, url_for
from front.forms import UserSignupForm, UserLoginForm
from .models import FrontUser, BannerModel, BoardModel, PostModel, CommentModel
from exts import db
from front.login_required import login_required
from cms import cms_bp
from flask_paginate import Pagination, get_page_parameter
front_bp = Blueprint('front', __name__)
import json
import config
import pymongo
clint = pymongo.MongoClient()
db1 = clint.dangdang
books = db1.jsjwl


@front_bp.route('/', endpoint='index')
@login_required
def index():
    """
    sort = request.args.get('sort', type=int, default=1)
    if sort == 1:
        my_query_obj = PostModel.query
    elif sort == 2:
        my_query_obj = db.session.query(PostModel).outerjoin(
            CommentModel).group_by(PostModel.id).order_by(
            db.func.count(CommentModel.id).desc())
    # 获取板块id
    bd = request.args.get('bd', type=int, default=None)

    page = request.args.get(get_page_parameter(), type=int, default=1)
    start = (page-1)*config.PER_PAGE
    end = start+config.PER_PAGE

    posters = None
    total = 0
    if bd:
        query_obj = my_query_obj.filter(PostModel.border_id == bd)
        posters = query_obj.slice(start, end).all()
        total = query_obj.count()
    else:
        query_obj = my_query_obj
        posters = query_obj.slice(start, end).all()
        total = query_obj.count()

    pagination = Pagination(bs_version=3, page=page, per_page=config.PER_PAGE, total=total)

    context = {
        'posters': posters,
        'current_bd': bd,
        'pagination': pagination
    }
    return render_template('front/index.html', **context)
    """
    page = request.args.get(get_page_parameter(), type=int, default=1)
    skip_obj = (page - 1)*config.PER_PAGE
    total = books.count({})
    info = books.find({}, {'_id': 0, 'title': 1}).skip(skip_obj).limit(config.PER_PAGE)
    li = []
    for i in info:
        li.append(i)
    pagination = Pagination(bs_version=3, page=page, per_page=config.PER_PAGE, total=total)

    context = {
        'info': li,
        'pagination': pagination
    }
    return render_template('front/index.html', **context)

@front_bp.context_processor
@cms_bp.context_processor
def tpl_extra():
    user_id = session.get('user_id')
    user = db.session.query(FrontUser).filter(FrontUser.username == user_id).first()
    banners = db.session.query(BannerModel).all()
    boards = BoardModel.query.all()
    id = request.args.get('id')
    data = dict(
        user=user,
        banners=banners,
        boards=boards,
        id=id
    )
    return data


class SingUpView(views.MethodView):
    def get(self):
        return render_template('front/signup.html')

    def post(self):
        form = UserSignupForm(request.form)
        if form.validate():
            username = form.username.data
            pwd1 = form.pwd1.data
            deposit = form.deposit.data
            avatar = form.avatar.data
            front_user = FrontUser(username=username, password=pwd1, deposit=deposit, avatar=avatar)
            db.session.add(front_user)
            db.session.commit()
            session['user_id'] = front_user.username
            # session.permanent = True
            return {'code': 200, 'message': '注册成功'}
        else:
            for i in form.errors:
                error1 = form.errors[i]
            return {'code': 400, 'message': error1}


class LoginView(views.MethodView):
    def get(self, msg=''):
        return render_template('front/login.html', msg=msg)

    def post(self):
        form = UserLoginForm(request.form)
        if form.validate():
            username = form.username.data
            password = form.password.data
            # print(password)
            user = FrontUser.query.filter_by(username=username).first()
            if user:
                result = user.check_password(password)
                if result is False:
                    return {'code': 400, 'message': '密码错误'}
                session['user_id'] = username
                session.permanent = True
                return {'code': 200, 'message': '登录成功'}
            else:
                return {'code': 400, 'message': '用户不存在'}
        else:
            msg = form.errors
            print(msg)
            return {'code': 400, 'message': msg}


@cms_bp.route('/exit/')
@front_bp.route('/exit/')
def exit():
    session.pop('user_id')
    return redirect(url_for('front.index'))


@front_bp.route('/books/')
def get_books():
    li = []
    for i in books.find({},{'_id': 0}).limit(100):
        li.append(i)
    return render_template('front/books.html', li=li)


class Deposit(views.MethodView):
    def get(self):
        return render_template('front/deposit.html')

    def post(self):
        money = request.form.get('money')
        password = request.form.get('password')
        user_id = session.get('user_id')
        user = db.session.query(FrontUser).filter(FrontUser.username == user_id).first()
        if user.check_password(password):
            user.deposit = user.deposit + float(money)
            db.session.add(user)
            db.session.commit()
            return {'code': 200, 'message': '充值成功,你的余额为%s元' % user.deposit}
        return {'code': 400, 'message': '密码错误,充值失败,你的余额为%s元' % user.deposit}


front_bp.add_url_rule('/signup/', view_func=SingUpView.as_view('signup'))
front_bp.add_url_rule('/login/', view_func=LoginView.as_view('login'))
front_bp.add_url_rule('/deposit/', view_func=Deposit.as_view('deposit'))
