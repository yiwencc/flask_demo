# -*- coding = utf-8 -*-
# @Time : 2020/7/29 10:16
# @Author : 贾伟文
# @File : forms.py
# @Software : PyCharm

from wtforms import Form, StringField, FloatField
from wtforms.validators import Length, Regexp, InputRequired, EqualTo, NumberRange


class UserSignupForm(Form):
    username = StringField(validators=[Regexp(r'\w{2,20}')])
    pwd1 = StringField(validators=[Regexp(r'.{2,20}')])
    pwd2 = StringField(validators=[EqualTo('pwd1')])
    deposit = FloatField(validators=[NumberRange(1, 1000000)])
    avatar = StringField()


class UserLoginForm(Form):
    username = StringField(validators=[Regexp(r'\w{2,20}')])
    password = StringField(validators=[Regexp(r'.{2,20}')])
