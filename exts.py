# -*- coding = utf-8 -*-
# @Time : 2020/7/29 9:47
# @Author : 贾伟文
# @File : exts.py
# @Software : PyCharm

from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()
