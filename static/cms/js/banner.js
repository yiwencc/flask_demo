$(function () {
    $('#banner_save_btn').click(function () {
        var self = $(this)

        var dialog = $('#banner_dialog')

        var name_input = dialog.find('input[name=name]');
        var image_url_input = dialog.find('input[name=image_url]');
        var link_url_input = dialog.find('input[name=link_url]');
        var priority_input = dialog.find('input[name=priority]');

        var name = $('input[name=name]').val();
        var image_url = $('input[name=image_url]').val();
        var link_url = $('input[name=link_url]').val();
        var priority = $('input[name=priority]').val();

        var id = dialog.attr('data-id')
        var update = dialog.attr('data-update')

        // console.log(name);
        // console.log(image_url);
        // console.log(link_url);
        // console.log(priority);

        if(!name | !image_url |!link_url | !priority){
            xtalert.alertErrorToast('请输入完整的轮播图数据')
        }
        url = ''
        if(update){
            url = 'http://admin.jww.com:5000/u_banner/'
        }else{
            url = 'http://admin.jww.com:5000/add_banner/'
        }
        console.log(url);
        myajax.post({
            'url':url,
            'data':{
                name:name,
                image_url:image_url,
                link_url:link_url,
                priority:priority,
                data_id:id
            },
            success:function (data) {
                if(data['code']==200){
                    location.reload()
                }else{
                    xtalert.alertError(message=data['message'])
                }
            },
            fail:function () {
                xtalert.alertNetworkError()
            }
        })
    })
})

//修改
$(function () {
    $('.edit_banner').click(function () {
        var self = $(this)
        var tr = self.parent().parent()
        var dialog = $('#banner_dialog')
        dialog.modal('show')

        var name_input = dialog.find('input[name=name]');
        var image_url_input = dialog.find('input[name=image_url]');
        var link_url_input = dialog.find('input[name=link_url]');
        var priority_input = dialog.find('input[name=priority]');

        name_input.val(tr.attr('data-name'));
        image_url_input.val(tr.attr('data-image'));
        link_url_input.val(tr.attr('data-link'));
        priority_input.val(tr.attr('data-priority'));

        dialog.attr({'data-id':tr.attr('data-id')})
        dialog.attr({'data-update':'update'})

    })
})

//删除
$(function () {
    $('.delete_banner').click(function (e) {
        e.preventDefault();
        var self = $(this)
        var tr = self.parent().parent()
        xtalert.alertConfirm({'msg':'您确定要删除吗？','confirmCallback':function () {
                id = tr.attr('data-id')
                console.log(id);
                myajax.post({
                    url:'http://admin.jww.com:5000/del_banner/',
                    data:{
                        data_id:id
                    },
                    success:function () {
                        window.location.reload()
                    },
                    fail:function () {
                        xtalert.alertNetworkError()
                    }
                })
            }})
    })
})

//七牛
window.onload = function () {
         qiniu.setUp({
             'domain': 'qjlwvxsyf.hd-bkt.clouddn.com//',
             'browse_btn': 'upload_btn',
             'uptoken_url': '/uptoken/',
             'success': function (up,file,info) {
                 var file_url = file.name;
                 console.log(file_url);
                 $('input[name=image_url]').val(file_url);
                 $('input[name=link_url]').val('http://'+file_url);
             }
         });
 };
