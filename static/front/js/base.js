$(function () {
    $('#trans_btn').click(function (e) {
        e.preventDefault();
        var username_input = $('input[name=username]');
        var money_input = $('input[name=money]');
        var password_input = $('input[name=password]');

        var username = username_input.val();
        var money = money_input.val();
        var password = password_input.val();

        myajax.post({
            'url':'/trans/',
            // type:'POST',
            data:{
                username:username,
                money:money,
                password:password
            },
            success:function (res) {
                if (res['code'] == 400){
                    xtalert.alertErrorToast(res['message']);
                    $(function () {
                        setTimeout("window.location.reload()", 1000)
                    })
                }
                else {
                    xtalert.alertSuccessToast(res['message']);
                    $(function () {
                        setTimeout("window.location.reload()", 1000)
                    })
                }
            },
            error:function () {
                console.log('网络波动');
            }
        })
    });
    $('.info_button').click(function (e) {
        var title = $(this).attr('title');
        var descs = $(this).attr('descs');
        xtalert.alertInfoWithTitle(title, descs)
    })

});
