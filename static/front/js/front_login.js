$(function () {
    $('#login_btn').click(function (e) {
        e.preventDefault();
        var username_input = $('input[name=username]');
        var password_input = $('input[name=password]');
        // var csrf_token = $('input[name=csrf_token]').val();

        var username = username_input.val();
        var password = password_input.val();

        myajax.post({
            'url': '/login/',
            // 'type':'POST',
            'data': {
                // csrf_token:csrf_token,
                username:username,
                password:password
            },
            success:function (res) {
                if (res['code']==200){
                    xtalert.alertSuccess(res['message']);
                    $(function(){ setTimeout("window.location='/?id=1'",1000);})
                }
                else {
                    xtalert.alertErrorToast(res['message']);
                    // window.location.reload()
                    $(function () {
                        setTimeout("window.location.reload()", 1000)
                    })
                }
            },
            error:function () {
                xtalert.alertNetworkError()
            }
        })

    })
});