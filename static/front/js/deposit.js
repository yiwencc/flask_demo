$(function () {
    $('#deposit_btn').click(function (e) {
        e.preventDefault();

        var money_input = $('input[name=money]');
        var password_input = $('input[name=password]');

        var money = money_input.val();
        var password = password_input.val();

        myajax.post({
            url:'/deposit/',
            // type:'POST',
            data:{
                money:money,
                password:password
            },
            success:function (res) {
                if (res['code'] == 200){
                    xtalert.alertSuccessToast(res['message'])
                    $(function () {
                        setTimeout("window.location.reload()", 1000)
                    })
                }
                else {
                    xtalert.alertErrorToast(res['message'])
                }
            },
            error:function () {
                xtalert.alertNetworkError()
            }
        })
    })

});