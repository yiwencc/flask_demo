$(function () {
    $('#submit_btn').click(function (e) {
        e.preventDefault();
        var username_input = $('input[name=username]');
        var pwd1_input = $('input[name=pwd1]');
        var pwd2_input = $('input[name=pwd2]');
        var deposit_input = $('input[name=deposit]');
        var avatar_url_input = $('input[name=avatar_url]');

        var username = username_input.val();
        var pwd1 = pwd1_input.val();
        var pwd2 = pwd2_input.val();
        var deposit = deposit_input.val();
        var avatar = avatar_url_input.val();
        if (!username|!pwd1|!pwd2|!deposit|!avatar){
            return false
        }
        console.log(username);
        myajax.post({
            'url':'/signup/',
            // 'type':'POST',
            'data':{
                username:username,
                pwd1:pwd1,
                pwd2:pwd2,
                deposit:deposit,
                avatar:avatar
            },
            'success':function (res) {
                if (res['code']==200){
                    xtalert.alertSuccess(res['message']);
                    $(function(){ setTimeout("window.location='/'",1000);})
                }
                else {
                    xtalert.alertErrorToast(res['message']);
                    $(function () {
                        setTimeout("window.location.reload()", 1000)
                    })
                }
            },
            'error':function () {
                console.log('网络故障');
            }
        })

    })
});

window.onload = function () {
         qiniu.setUp({
             'domain': 'qjlwvxsyf.hd-bkt.clouddn.com//',
             'browse_btn': 'upload-btn',
             'uptoken_url': '/uptoken/',
             'success': function (up,file,info) {
                 var file_url = file.name;
                 console.log(file_url);
                 $('input[name=avatar_url]').val('http://'+file_url);
                 $('#img').attr({src:'http://'+file_url})
             }
         });
 };
