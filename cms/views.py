# -*- coding = utf-8 -*-
# @Time : 2020/8/5 13:12
# @Author : 贾伟文
# @File : views.py
# @Software : PyCharm

from flask import Blueprint, render_template, request
from front.models import BannerModel
from exts import db

cms_bp = Blueprint('cms', __name__, subdomain='admin')


@cms_bp.route('/')
def index():
    return render_template('cms/index.html')


@cms_bp.route('/del_banner/', methods=['POST'])
def del_banner():
    id = request.form.get('data_id')
    if id:
        user = BannerModel.query.get(id)
        db.session.delete(user)
        db.session.commit()
        return {'code': 200}


@cms_bp.route('/u_banner/', methods=['POST'])
def u_banner():
    id = request.form.get('data_id')
    name = request.form.get('name')
    image = request.form.get('image_url')
    link = request.form.get('link_url')
    priority = request.form.get('priority')
    user = BannerModel.query.get(id)
    user.name = name
    user.image_url = image
    user.link_url = link
    user.priority = priority
    db.session.commit()
    return {'code': 200}


@cms_bp.route('/add_banner/', methods=['POST'])
def add_banner():
    name = request.form.get('name')
    image = request.form.get('image_url')
    link = request.form.get('link_url')
    priority = request.form.get('priority')

    banner = BannerModel(name=name, image_url=image, link_url=link, priority=priority)
    db.session.add(banner)
    db.session.commit()
    return {'code': 200}
