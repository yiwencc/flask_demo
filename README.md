# 项目使用 

## [项目预览地址](http://yiwencc.cn)


## [博客地址](https://www.cnblogs.com/YiwenGG/)

## 创建虚拟环境

`
mkvirtualenv flask_books
`


## 自动安装依赖包

`
pip install -r requirements.txt
`


## 打开项目在根目录下更改config配置文件中mysql为自己的数据库账户密码
   
## 进行数据库迁移
```python
python manage.py db init
python manage.py db migrate
python manage.py db upgrade
```

## 迁移成功之后运行即可，主页和书籍页数据来自mongodb,需在front/views文件夹下更改自己阅读代码即可！！！


## 运行项目

`
python app.py
`




