# -*- coding = utf-8 -*-
# @Time : 2020/7/29 8:59
# @Author : 贾伟文
# @File : config.py
# @Software : PyCharm

from datetime import timedelta

DEBUG = False
DB_URI = 'mysql://root:123456@localhost:3306/icbc?charset=utf8'
SQLALCHEMY_DATABASE_URI = DB_URI
SQLALCHEMY_TRACK_MODIFICATIONS = False
SECRET_KEY = 'abfhdj'
# SERVER_NAME = 'jww.com:80'
PERMANENT_SESSION_LIFETIME = timedelta(days=10)
PER_PAGE = 8
