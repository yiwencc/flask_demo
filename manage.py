# -*- coding = utf-8 -*-
# @Time : 2020/7/29 9:59
# @Author : 贾伟文
# @File : manage.py
# @Software : PyCharm

from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from app import app
from exts import db
from front import BoardModel, PostModel

manager = Manager(app)
Migrate(app, db)
manager.add_command('db', MigrateCommand)


# 创建板块
@manager.command
def create_board():
    board = BoardModel(name='python')
    db.session.add(board)
    db.session.commit()
    print('created %s board success' % board.name)


# 创建帖子
@manager.command
def create_poster():
    for i in range(1, 201):
        poster = PostModel(title='板块3标题%s' % i,
                  content='板块3内容%s' % i)
        poster.board = BoardModel.query.get(3)
        db.session.add(poster)
        db.session.commit()
        print('Inserted %d pieces of data successfully' % i)


if __name__ == '__main__':
    manager.run()
